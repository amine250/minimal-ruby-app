### Simple Ruby Web service
This application only purpose is to return simple string 
in response to HTTP request on port 5000


### Dockerfile
Application is shipped in thin Docker container based on 
`ruby:2.4.0-alpine`.
  

```Dockerfile
FROM ruby:2.4.0-alpine
ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
```


### Application
Simplest ruby web app that doesn't use any external dependencies 
in order to speed up the build time.

```ruby
require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

server.mount_proc '/' do |request, response|
  response.body = 'Hello, world!'
end

server.start
```